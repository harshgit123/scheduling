import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jpa.EmployeeController2;
import com.jpa.UserData;

@RunWith(SpringRunner.class)
@WebMvcTest(value = EmployeeController2.class, secure = false)
public class ProductServiceControllerTest {
	@Autowired
	private MockMvc mvc;

	protected <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}

	@Test
	public void getProductsList() throws Exception {
		String uri = "/test1";
		/*
		 * // MvcResult mvcResult =
		 * mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.
		 * APPLICATION_JSON_VALUE)) // .andReturn(); // // int status =
		 * mvcResult.getResponse().getStatus(); // assertEquals(200, status); // String
		 * content = mvcResult.getResponse().getContentAsString(); // UserData userData
		 * = mapFromJson(content, UserData.class); // assertEquals("harsh",
		 * userData.getFirstName());
		 */	}
	/*
	 * @Test public void createProduct() throws Exception { String uri =
	 * "/products"; Product product = new Product(); product.setId("3");
	 * product.setName("Ginger"); String inputJson = super.mapToJson(product);
	 * MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	 * .contentType(MediaType.APPLICATION_JSON_VALUE)
	 * .content(inputJson)).andReturn();
	 * 
	 * int status = mvcResult.getResponse().getStatus(); assertEquals(201, status);
	 * String content = mvcResult.getResponse().getContentAsString();
	 * assertEquals(content, "Product is created successfully");
	 * 
	 * }
	 * 
	 * @Test public void updateProduct() throws Exception { String uri =
	 * "/products/2"; Product product = new Product(); product.setName("Lemon");
	 * String inputJson = super.mapToJson(product); MvcResult mvcResult =
	 * mvc.perform(MockMvcRequestBuilders.put(uri)
	 * .contentType(MediaType.APPLICATION_JSON_VALUE)
	 * .content(inputJson)).andReturn();
	 * 
	 * int status = mvcResult.getResponse().getStatus(); assertEquals(200, status);
	 * String content = mvcResult.getResponse().getContentAsString();
	 * assertEquals(content, "Product is updated successsfully"); }
	 * 
	 * @Test public void deleteProduct() throws Exception { String uri =
	 * "/products/2"; MvcResult mvcResult =
	 * mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn(); int status =
	 * mvcResult.getResponse().getStatus(); assertEquals(200, status); String
	 * content = mvcResult.getResponse().getContentAsString(); assertEquals(content,
	 * "Product is deleted successsfully"); }
	 */
}