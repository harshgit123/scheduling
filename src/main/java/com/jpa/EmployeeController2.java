package com.jpa;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController(value = "/api")
@Component
public class EmployeeController2 {

	@Autowired
	ScheduleTask task;
	@Autowired
	UserDataDao dao;
    @PostMapping(value = "/schedule")
	public void showTask(@RequestBody UserData user) {
    	//user.setPostingDate(new Date());
	//	task.checkMethod(user);
    	dao.save(user);
	}
	
    @GetMapping("/test1")
    public ResponseEntity<UserData> getUserData() {
    	UserData data = new UserData();
    	data.setId(123);
    	data.setFirstName("harsh");
    	return new ResponseEntity<UserData>(data, HttpStatus.OK);
    }
	
}
