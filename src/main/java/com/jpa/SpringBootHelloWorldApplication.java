package com.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

//@EnableWebMvc
@EnableScheduling
@SpringBootApplication
public class SpringBootHelloWorldApplication {

	@Autowired
	private ConfigDataDao cronRepo;

	@Bean
	public String getCronValue()
	{
	    return cronRepo.findByKey("cron").get(0).getKey();
	}
	@Autowired
	static
	ScheduleTask task;
	public static void main(String[] args) {
		SpringApplication.run(SpringBootHelloWorldApplication.class, args);
	}
	
	
}
