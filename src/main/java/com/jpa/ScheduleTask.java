package com.jpa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduleTask {

	public static final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);

	@Autowired
	UserDataDao dao;

	public void checkMethod(UserData user) {
		Date now = new Date();
		Date date = user.getPostingDate();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String postingDate = format.format(date);
		String currentDate = format.format(now);
		if (currentDate.compareTo(postingDate) == 0) {
			System.out.println("Task schedule at time :--" + date);// 1551922980000-1551966133295
			scheduledExecutorService.schedule(new RunTest(user), (date.getTime() - (new Date().getTime())),
					TimeUnit.MILLISECONDS);
		}
		// insert in db all the data
		dao.save(user);
	}

	class RunTest implements Runnable {

		UserData user;

		public RunTest(UserData user) {

			this.user = user;
		}

		public void run() {
			System.out.println("in run method......" + new Date());
			// code to call the api's
			user.setData("updated At: " + new Date());
			dao.save(user);
			System.out.println("Data saved.");

		}

	}

	class FetchData implements Runnable {

		public void run() {
			System.out.println("in FetchData method......");
			// code to fetch the todays data from db
			Iterable<UserData> userData = dao.findAll();
			for (UserData user : userData) {
				scheduledExecutorService.schedule(new RunTest(user),
						(user.getPostingDate().getTime() - (new Date().getTime())), TimeUnit.MILLISECONDS);
			}
		}

	}

	class TimerTaskData1 extends TimerTask {
		UserData user;

		public TimerTaskData1(UserData user) {

			this.user = user;
		}

		@Override
		public void run() {
			System.out.println("in run method......" + new Date());
			// code to call the api's
			user.setData("updated At: " + new Date());
			dao.save(user);
			System.out.println("Data saved.");

		}

	}

	class TimerTaskData {

		public void run() {
			System.out.println("in FetchData method......");
			// code to fetch the todays data from db
			Iterable<UserData> userData = dao.findAll();
			for (UserData user : userData) {
				TimerTaskData1 task = new TimerTaskData1(user);
				Timer timer = new Timer("Timer");
				timer.schedule(task, user.getPostingDate());
				System.out.println("total thread :-" + Thread.activeCount());
				System.out.println("Current thread name is:- " + Thread.currentThread().getName());
			}
		}

	}

	public void fixedRateThread() {
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		int delayInHour = hour < 20 ? 20 - hour : 24 - (hour - 20);

		System.out.println("Current Hour: " + hour);
		System.out.println("Comuted Delay for next 7 PM: " + delayInHour);

		scheduledExecutorService.scheduleAtFixedRate(new FetchData(), delayInHour, 24, TimeUnit.HOURS);
	}

	@Scheduled(cron = "0 40 20 * * ?")
	// @Scheduled(cron = "#{@getCronValue} 13 21 * * ?")
//	@Scheduled(cron = "${cron.config}")
	public void performTaskUsingCron() {

		System.out.println(new Date().getTime() + "  Regular task performed using Cron at " + new Date());
		System.out.println("Comuted Delay for next 7 PM: ");
		/*
		 * TimerTaskData data = new TimerTaskData(); data.run();
		 */

		FetchData data = new FetchData();
		data.run();

	}
}
